#ifndef __DEFINE_H
#define __DEFINE_H

#define Flash_Latency_2         2
#define ADC1_DR_Address         ((uint32_t)0x4001244C)
#define DMA_Buffer_Size         1
#define DMA_DIR_PeriphialSRC    0
#define Red_Led                 GPIO_Pin_11
#define Green_Led               GPIO_Pin_12


#endif
