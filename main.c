/**
******************************************************************************
* @file HRV2000-PRO.c
* @authors Ges Teknik R&D Team
* @date 28-Eylül-2016
* @version V0.0.1
* @copyright Copyright (c) Yiido
* @brief BlaBla
C0-Analog
A0-Status LED
******************************************************************************
*/

/*****************************************************************************/
/* INCLUDES */
/*****************************************************************************/
#include <stm32f10x.h>
#include <stm32f10x_rcc.h>                                                                                             
#include <stm32f10x_gpio.h>
#include <Define.h>

/*****************************************************************************/
/* GLOBALS */
/*****************************************************************************/
uint32_t Dma_Buffer;
uint16_t led = 0;

GPIO_InitTypeDef GPIO_InitStructure;
ErrorStatus HSEStartUpStatus ;
DMA_InitTypeDef DMA_InitStructure;

/*****************************************************************************/
/* FUNCTIONS */
/*****************************************************************************/
void Rcc_init(
    void);
void InitGPIO(
    void);
void AdcInit(
    void);
void DMa_Init(
    void);
void Delay(
    uint32_t ncount);
void SysTickConfig(
    void);

int main()
{
  Rcc_init();
  InitGPIO();
  AdcInit();
  DMa_Init();
 

  for(led = 0; led <= 3; led++){
    
    GPIO_SetBits(GPIOA, Red_Led);
    GPIO_SetBits(GPIOA, Green_Led);
    Delay(500);
    GPIO_ResetBits(GPIOA, Red_Led);
    GPIO_ResetBits(GPIOA, Green_Led);
    Delay(500);
  }
  
   SysTickConfig();
  
  while(1)
  {}
}

/**
  * @brief   RCC Ayarlanması
  * @param  None
  * @retval None
  */
void Rcc_init(void)
{ 
  RCC_DeInit();
  
  RCC_HSEConfig(RCC_HSE_ON);
  
  HSEStartUpStatus = RCC_WaitForHSEStartUp();
  
  if ( HSEStartUpStatus == SUCCESS) {
    
    RCC_HCLKConfig(RCC_SYSCLK_Div1);
    
    RCC_PCLK2Config(RCC_HCLK_Div1);
    
    RCC_PCLK1Config(RCC_HCLK_Div2);
    
    FLASH_SetLatency(Flash_Latency_2);

    FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);                                                              /* NOTE: Enable Prefetch Buffer */
   
    RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);                                                               /* NOTE: PLLCLK = 8MHz * 9 = 72 MHz */ 
    
    RCC_PLLCmd(ENABLE);                                                                                                /* NOTE: Enable PLL */
    
    while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);                                                               /* NOTE: Wait untill PLL is ready */
    
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);                                                                         /* NOTE: Select PLL as system clock source */
    
    while (RCC_GetSYSCLKSource() != 0x08);                                                                             /* NOTE: Wait untill PLL is used as system clock source */
    
    RCC_MCOConfig(RCC_MCO_SYSCLK);
  
  }
}

/**
  * @brief  Gpio Ayarlanması
  * @param  None
  * @retval None
  */
void InitGPIO(void)
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC, ENABLE);
  
  GPIO_InitStructure.GPIO_Mode                  = GPIO_Mode_Out_PP ;
  GPIO_InitStructure.GPIO_Pin                   = GPIO_Pin_11 | GPIO_Pin_12;
  GPIO_InitStructure.GPIO_Speed                 = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  GPIO_InitStructure.GPIO_Mode                  = GPIO_Mode_AIN;
  GPIO_InitStructure.GPIO_Pin                   = GPIO_Pin_0;
  GPIO_Init(GPIOC, &GPIO_InitStructure);  
}

/**
  * @brief  Adc Ayarlanması
  * @param  None
  * @retval None
  */
void AdcInit(void)
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
  
  ADC_InitTypeDef ADC_InitStructure;
  ADC_InitStructure.ADC_Mode                    = ADC_Mode_Independent;
  ADC_InitStructure.ADC_ScanConvMode            = ENABLE;
  ADC_InitStructure.ADC_ContinuousConvMode      = ENABLE;
  ADC_InitStructure.ADC_DataAlign               = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_ExternalTrigConv        = ADC_ExternalTrigConv_None;
  ADC_InitStructure.ADC_NbrOfChannel            = 1;
  ADC_Init(ADC1, &ADC_InitStructure);
  
  ADC_Cmd(ADC1, ENABLE); 

  ADC_ResetCalibration(ADC1);
  while(ADC_GetResetCalibrationStatus(ADC1));

  ADC_StartCalibration(ADC1);
  while(ADC_GetCalibrationStatus(ADC1));
  ADC_RegularChannelConfig(ADC1,ADC_Channel_0, 1, ADC_SampleTime_239Cycles5);
}

/**
  * @brief  DMa Ayarlanması
  * @param  None
  * @retval None
  */
void DMa_Init(void)
{
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
  
  DMA_DeInit(DMA1_Channel1);
  DMA_InitStructure.DMA_PeripheralBaseAddr      = ADC1_DR_Address;
  DMA_InitStructure.DMA_BufferSize              = DMA_Buffer_Size;
  DMA_InitStructure.DMA_DIR                     = DMA_DIR_PeriphialSRC;
  DMA_InitStructure.DMA_M2M                     = DMA_M2M_Disable;
  DMA_InitStructure.DMA_MemoryBaseAddr          = (uint32_t)&Dma_Buffer;
  DMA_InitStructure.DMA_MemoryDataSize          = DMA_MemoryDataSize_HalfWord;
  DMA_InitStructure.DMA_MemoryInc               = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_Mode                    = DMA_Mode_Normal;
  DMA_InitStructure.DMA_PeripheralDataSize      = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_PeripheralInc           = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_Priority                = DMA_Priority_High;
  DMA_Init(DMA1_Channel1, &DMA_InitStructure);
  
  DMA_Cmd(DMA1_Channel1, ENABLE);
  ADC_DMACmd(ADC1, ENABLE);
  
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}

void Delay(uint32_t ncount)
{
  __IO uint32_t inside = 0;
  
  for(inside = (ncount*7200); inside != 0; inside--  );
}

void SysTickConfig(void)
{
  if((SysTick_Config(SystemCoreClock / 1000)) != 0){}
}

